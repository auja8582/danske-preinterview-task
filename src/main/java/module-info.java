open module danske.preinterview.task {
    requires spring.context;
    requires spring.core;
    requires spring.web;
    requires spring.webmvc;
    requires org.apache.logging.log4j;
    requires spring.beans;
    requires spring.boot;
    requires spring.boot.autoconfigure;
}