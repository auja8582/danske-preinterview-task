package com.danske.game.configuration;

import com.danske.game.model.PuzzleAction;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class GameControllerConfiguration {
    @ControllerAdvice
    class GlobalControllerExceptionHandler {
        @ExceptionHandler(ConversionFailedException.class)
        public ResponseEntity<String> handleConflict(RuntimeException ex) {
            return new ResponseEntity<>(ex.getMessage(),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @Component
    class WebConfig implements WebMvcConfigurer {
        @Override
        public void addFormatters(FormatterRegistry registry) {
            //need to map enum types to pathVariables
            registry.addConverter(new StringToEnumConverter());
        }
    }

    @Component
    class StringToEnumConverter implements Converter<String, PuzzleAction> {
        @Override
        public PuzzleAction convert(String source) {
            return PuzzleAction.valueOf(source.toUpperCase());
        }
    }

}