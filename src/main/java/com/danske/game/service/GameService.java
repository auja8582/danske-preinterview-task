package com.danske.game.service;

import com.danske.game.exception.IllegalPuzzleActionException;
import com.danske.game.exception.NoSuchPuzzleException;
import com.danske.game.model.Puzzle;
import com.danske.game.model.PuzzleAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/*
 * NOTES
 *
 * Logging should be implemented via Spring AOP or even better via Aspectj as it
 * is considered to be a business cross-cutting concern. However for easier
 * readability let us do logging in the same methods we execute business logic
 *
 * To be honest mixing logs and business logic is not a good idea when you have
 * tools such as Spring AOP or Aspectj
 * */
@Service
public class GameService {
    public static final Map<Long, Puzzle> games = new ConcurrentHashMap<>();
    public static final AtomicLong incrementer = new AtomicLong();
    public static final Logger logger = LogManager.getLogger(GameService.class);

    public Long createPuzzle(){
        Long gameId = 0L;
        while (games.containsKey(incrementer.get())) {
            logger.debug("checking if index: {} is unique",
                    incrementer.get());
            gameId = incrementer.incrementAndGet();
        }
        games.put(gameId, new Puzzle());
        logger.debug("new game with id: {} was created.", gameId);
        return gameId;
    }

    public Puzzle getPuzzleState(Long puzzleId) throws NoSuchPuzzleException {
        Puzzle puzzle = games.get(puzzleId);
        if (puzzle == null) {
            throw new NoSuchPuzzleException();
        }
        return puzzle;
    }

    public Puzzle updatePuzzleState(Long puzzleId, PuzzleAction action)
            throws NoSuchPuzzleException, IllegalPuzzleActionException {
        Puzzle puzzle = games.get(puzzleId);
        if (puzzle == null) {
            throw new NoSuchPuzzleException();
        }
        /*As an algorithmic correctness is less important and we DO NOT need to
        implement a game solver we are free to skip this part, but the general
        pseudo algorithm would be

        1. check if an update is valid, i.e if action is UP check that
           we can actually move UP, if not valid throw an exception
        2. Execute puzzle action
        3. Update games(puzzle map) object
        4. Return updated object
        */
        return puzzle;
    }

    public Puzzle deletePuzzle(Long puzzleId) throws NoSuchPuzzleException {
        Puzzle puzzle = null;
        if ((puzzle = games.remove(puzzleId)) == null) {
            throw new NoSuchPuzzleException();
        }
        return puzzle;
    }

    /*we could also create isSolved method, for example, to test if the puzzle
     *is solved. But let us just skip this step for the sake of simplicity
     *assume that it is users responsibility to decide whether or not the puzzle
     *is solved.
     */
}
