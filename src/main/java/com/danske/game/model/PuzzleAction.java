package com.danske.game.model;

/* As per a given task we are not required to do an implementation of an actual
 * game as algorithmic correctness is less important and we do NOT need to
 * create a game solver. So let as do some random mocking in testing
 * for the proof of a concept without implementing PuzzleAction object.
 *
 * P.S
 *
 * A shy implementation of possible actions can be seen bellow.
 * */
public enum PuzzleAction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
