package com.danske.game.model;

import java.util.ArrayList;
import java.util.List;

/* As per a given task we are not required to do an implementation of an actual
 * game as algorithmic correctness is less important and we do NOT need to
 * create a game solver. So let as do some random mocking in testing
 * for the proof of a concept without implementing puzzle object.
 * */
public class Puzzle {
    //only for mocking purposes
    private List<String> state = new ArrayList<>();

    public List<String> getState() {
        return state;
    }

    public void setState(List<String> state) {
        this.state = state;
    }
}
