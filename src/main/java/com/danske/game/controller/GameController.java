package com.danske.game.controller;

import com.danske.game.exception.IllegalPuzzleActionException;
import com.danske.game.exception.NoSuchPuzzleException;
import com.danske.game.model.Puzzle;
import com.danske.game.model.PuzzleAction;
import com.danske.game.service.GameService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/*
 * NOTES
 *
 * Logging should be implemented via Spring AOP or even better via Aspectj as it
 * is considered to be a business cross-cutting concern. However for easier
 * readability let us do logging in the same methods we execute business logic
 *
 * To be honest mixing logs and business logic is not a good idea when you have
 * tools such as Spring AOP or Aspectj
 *
 * ALSO
 *
 * for an enhanced logging experience we could use something like Spring Sleuth
 * or at least an interceptor to add uniqueness to the request. But for the
 * sake of simplicity these things were left alone.
 * */
@RestController
public class GameController {

    private GameService gameService;
    public static final Logger logger =
            LogManager.getLogger(GameController.class);

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = "/game")
    public ResponseEntity<?> createGame() {
        try {
            Long gameId = gameService.createPuzzle();
            logger.info("game with id: {} created successfully", gameId);
            return ResponseEntity.ok(gameId);
        } catch (Exception e) {
            /*anything that bad can happen would happen only because
            * of back-end (server side) issues*/
            logger.error("error while creating a game: ", e);
            return ResponseEntity.status(500).build();
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/game/{id}")
    public ResponseEntity<?> getGame(@PathVariable Long id) {
        try {
            Puzzle pzl = gameService.getPuzzleState(id);
            return ResponseEntity.ok(pzl);
        } catch (NoSuchPuzzleException e) {
            /*the requested puzzle might not be existent*/
            logger.error("failed to retrieve a game with an id: {} ", id, e);
            return ResponseEntity.notFound().build();
        } catch (Exception e){
            /*ALL other issues might happen ONLY on
            * server side*/
            logger.error("error while retrieving a game with id: {}", id, e);
            return ResponseEntity.status(500).build();
        }
    }

    @CrossOrigin(origins = "*")
    @PatchMapping(value = "/game/{id}/action/{action}")
    public ResponseEntity<?> updateGame(@PathVariable Long id,
                                        @PathVariable PuzzleAction action) {
        //TODO: for this endpoint to work correctly
        //we would need to implement the game logic itself
        //however as per a given task we are not required to do so, as
        //algorithmic correctness is less important and we do NOT need to
        //create a game solver. So let as do some random mocking in testing
        //for the proof of a concept.
        try {
            return ResponseEntity
                    .ok(gameService.updatePuzzleState(id, action));
        } catch (NoSuchPuzzleException e) {
            // no such puzzle object
            logger.error("failed to retrieve a game with an id: {} ", id, e);
            return ResponseEntity.notFound().build();
        } catch (IllegalPuzzleActionException e){
            // illegal move was send to a given puzzle
            logger.error("failed to perform an action {} on a game with " +
                    "an id: {} ", action, id, e);
            return ResponseEntity.badRequest().body("Illegal puzzle action");
        } catch (Exception e){
            // all other issues are ONLY server side related
            logger.error("error while performing an action {} on a game " +
                    "with an id: {} ", action, id, e);
            return ResponseEntity.status(500).build();
        }
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping(value = "/game/{id}")
    public ResponseEntity<?> deleteGame(@PathVariable Long id) {
        try {
            gameService.deletePuzzle(id);
            return ResponseEntity.ok(204);
        } catch (NoSuchPuzzleException e) {
            //no puzzle was found to be deleted
            logger.error("failed to delete a game with an id: {} ", id, e);
            return ResponseEntity.notFound().build();
        } catch (Exception e){
            //all other issues are solely back-end issues
            logger.error("error while deleting a game with an id: {} ",
                    id, e);
            return ResponseEntity.ok(500);
        }
    }
}
