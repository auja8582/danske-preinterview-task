package com.danske.game;

import com.danske.game.configuration.GameControllerConfiguration;
import com.danske.game.controller.GameController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {GameApplication.class,
        GameControllerConfiguration.class})
class GameApplicationTest {
    @Autowired
    private GameController gameController;

    @Test
    public void testContextLoads(){
        assertThat(gameController).isNotNull();
    }
}