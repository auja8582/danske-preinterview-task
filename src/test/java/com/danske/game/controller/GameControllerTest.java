package com.danske.game.controller;

import com.danske.game.GameApplication;
import com.danske.game.configuration.GameControllerConfiguration;
import com.danske.game.exception.IllegalPuzzleActionException;
import com.danske.game.exception.NoSuchPuzzleException;
import com.danske.game.model.Puzzle;
import com.danske.game.model.PuzzleAction;
import com.danske.game.service.GameService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@WebMvcTest(GameController.class)
@ContextConfiguration(classes = {GameApplication.class, GameControllerConfiguration.class})
class GameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private GameController gameController;

    @MockBean
    private Puzzle puzzle;

    @MockBean
    private GameService gameService;

    @Test
    void createGame_ReturnStatusIsOk() throws Exception {
        mockMvc.perform(post("/game")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is2xxSuccessful());
    }

    @Test
    void createGame_ReturnStatusIs500() throws Exception {
        given(gameService.createPuzzle())
                .willAnswer((s) -> {
                    throw new Exception();
                });
        mockMvc.perform(post("/game")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is5xxServerError());
    }

    @Test
    void getGame_ReturnStatusIsOk() throws Exception {
        when(gameService.getPuzzleState(1L)).thenReturn(new Puzzle());
        mockMvc.perform(get("/game/1")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is2xxSuccessful());
    }

    @Test
    void getGame_ReturnStatusIsNotFound() throws Exception {
        when(gameService.getPuzzleState(1L))
                .thenThrow(new NoSuchPuzzleException());
        mockMvc.perform(get("/game/1")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNotFound());
    }

    @Test
    void getGame_ReturnStatusIs500() throws Exception {
        given(gameService.getPuzzleState(1L))
                .willAnswer((s) -> {
                    throw new Exception();
                });
        mockMvc.perform(get("/game/1")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is5xxServerError());
    }

    @Test
    void updateGame_actionBasketCase_ReturnStatusIsOk() throws Exception {
        when(gameService.updatePuzzleState(1L, PuzzleAction.UP))
                .thenReturn(new Puzzle());
        mockMvc.perform(patch("/game/1/action/uP")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is2xxSuccessful());
    }

    @Test
    void updateGame_actionUpperCase_ReturnStatusIsOk() throws Exception {
        when(gameService.updatePuzzleState(1L, PuzzleAction.UP))
                .thenReturn(new Puzzle());
        mockMvc.perform(patch("/game/1/action/UP")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is2xxSuccessful());
    }

    @Test
    void updateGame_actionLowerCase_ReturnStatusIsOk() throws Exception {
        when(gameService.updatePuzzleState(1L, PuzzleAction.UP))
                .thenReturn(new Puzzle());
        mockMvc.perform(patch("/game/1/action/up")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is2xxSuccessful());
    }

    @Test
    void updateGame_actionLowerCase_ReturnStatusBadUserRequest()
            throws Exception {
        when(gameService.updatePuzzleState(1L, PuzzleAction.UP))
                .thenThrow(new IllegalPuzzleActionException());
        mockMvc.perform(patch("/game/1/action/up")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void updateGame_ReturnStatusIsNotFound() throws Exception {
        when(gameService.updatePuzzleState(1L, PuzzleAction.UP))
                .thenThrow(new NoSuchPuzzleException());
        mockMvc.perform(patch("/game/1/action/Up")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isNotFound());
    }

    @Test
    void updateGame_ReturnStatusIs500() throws Exception {
        given(gameService.updatePuzzleState(1L, PuzzleAction.UP))
                .willAnswer((s) -> {
                    throw new Exception();
                });
        mockMvc.perform(patch("/game/1/action/UP")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is5xxServerError());
    }

    @Test
    void deleteGame_statusIsOk() throws Exception {
        when(gameService.deletePuzzle(1L)).thenReturn(new Puzzle());
        mockMvc.perform(delete("/game/1")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is2xxSuccessful());
    }

    @Test
    void deleteGame_statusIs500() throws Exception {
        given(gameService.deletePuzzle(1L))
                .willAnswer((s) -> {
                    throw new Exception();
                });
        mockMvc.perform(delete("/game/1")
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().is2xxSuccessful());
    }
}