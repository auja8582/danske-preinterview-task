# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repo was set up for a pre interview task given by unkowns from Danske Bank 
* Original task can be found (https://bitbucket.org/auja8582/danske-preinterview-task/downloads/BackEnd_-_task_2.pdf)
* Job ad (https://bitbucket.org/auja8582/danske-preinterview-task/downloads/job_ad_danske_bank.pdf)

### How do I get set up? ###

# SETUP OVERVIEW #

* Compile the project
* Try rest endpoints (refer to test folder in the project)

### Compile the project ###

After you checked out the project to your local desktop. Import the project as a maven project in IDE you prefer.
execute clean -> build -> install. If all is ok then ok as all logic is mocked and no algorithm or algorithms
implemented (I doubt that my understanding on how I was supposed to do this project is correct.). 
Feedback is welcome.